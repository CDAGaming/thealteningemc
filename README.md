TheAltening EMC plugin
===================

This plugin adds support for [TheAltening](https://thealtening.com/) in Minecraft using EMC.

Usage
-------------------

This plugin only has command support at the moment. The commands to use this plugin are:

* `.altening ui` - Opens the UI for TheAltening.
* `.altening init "<api key>"` - initialises the plugin, you need to run this command before  using any other command.
* `.altening login "<token>"` - Logs into an account, with a token provided from TheAltening.
* `.altening generate` - Generates a random account and logs into it.
* `.altening service` - Prints the current auth service used, either "TheAltening" or "Mojang".
* `.altening service set <Mojang|TheAltening>` - Switches which auth service is used.

Installation
-------------------

* Clone this git
* Run `$ gradle build`
* Place the outputted jar in `.minecraft/libraries/EMC/<MC version>/`
* Start Minecraft

