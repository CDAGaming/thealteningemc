package me.deftware.thealtening;

import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.builder.RequiredArgumentBuilder;
import com.thealtening.AltService;
import me.deftware.client.framework.command.CommandBuilder;
import me.deftware.client.framework.command.CommandResult;
import me.deftware.client.framework.command.EMCModCommand;
import me.deftware.client.framework.utils.ChatProcessor;
import me.deftware.client.framework.wrappers.ISession;
import me.deftware.thealtening.ui.IGuiAltening;

@SuppressWarnings("ALL")
public class CommandAltening extends EMCModCommand {

    @Override
    public CommandBuilder getCommandBuilder() {
        return new CommandBuilder().set((LiteralArgumentBuilder) LiteralArgumentBuilder.literal("altening")
                .then(
                        LiteralArgumentBuilder.literal("ui")
                                .executes(c -> {
                                    Main.eventListener.screen = new IGuiAltening(null);
                                    return 1;
                                })
                )
                .then(
                        LiteralArgumentBuilder.literal("generate")
                                .executes(c -> {
                                    if (!AlteningAPI.initialized) {
                                        ChatProcessor.printFrameworkMessage("Please type '.altening init \"<api key>\"' before generating an account.");
                                        return 1;
                                    }
                                    ChatProcessor.printFrameworkMessage("Logging into an TheAltening account...");
                                    new Thread(() -> {
                                        try {
                                            if (AlteningAPI.currentService() == AltService.EnumAltService.MOJANG) {
                                                AlteningAPI.switchService(AltService.EnumAltService.THEALTENING);
                                            }
                                            AlteningAPI.currentAccount = AlteningAPI.genAccount();
                                            AlteningAPI.login(AlteningAPI.currentAccount);
                                            ChatProcessor.printFrameworkMessage("Logged into account " + ISession.getIUsername());
                                        } catch (Exception ex) {
                                            ex.printStackTrace();
                                            ChatProcessor.printFrameworkMessage("Failed to login to account");
                                        }
                                    }).start();
                                    return 1;
                                })
                )
                .then(
                        LiteralArgumentBuilder.literal("login")
                                .then(
                                        RequiredArgumentBuilder.argument("token", StringArgumentType.string())
                                                .executes(c -> {
                                                    CommandResult r = new CommandResult(c);
                                                    String token = r.getString("token");
                                                    if (!AlteningAPI.initialized) {
                                                        ChatProcessor.printFrameworkMessage("Please type '.altening init \"<api key>\"' before generating an account.");
                                                        return 1;
                                                    }
                                                    ChatProcessor.printFrameworkMessage("Logging into an TheAltening account...");
                                                    new Thread(() -> {
                                                        try {
                                                            if (AlteningAPI.currentService() == AltService.EnumAltService.MOJANG) {
                                                                AlteningAPI.switchService(AltService.EnumAltService.THEALTENING);
                                                            }
                                                            AlteningAPI.login(token);
                                                            ChatProcessor.printFrameworkMessage("Logged into account " + ISession.getIUsername());
                                                        } catch (Exception ex) {
                                                            ex.printStackTrace();
                                                            ChatProcessor.printFrameworkMessage("Failed to login to account");
                                                        }
                                                    }).start();
                                                    return 1;
                                                })
                                )
                )
                .then(
                        LiteralArgumentBuilder.literal("init")
                                .then(
                                        RequiredArgumentBuilder.argument("key", StringArgumentType.string())
                                                .executes(c -> {
                                                    CommandResult r = new CommandResult(c);
                                                    String key = r.getString("key");
                                                    Main.instance.getSettings().saveString("key", key);
                                                    Main.instance.getSettings().saveConfig();
                                                    if (!AlteningAPI.initialized) {
                                                        new Thread(() -> AlteningAPI.init(key)).start();
                                                        ChatProcessor.printFrameworkMessage("TheAltening is now ready for use.");
                                                    } else {
                                                        ChatProcessor.printFrameworkMessage("Please restart Minecraft to use the new api key.");
                                                    }
                                                    return 1;
                                                })
                                )
                )
                .then(
                        LiteralArgumentBuilder.literal("service")
                                .executes(c -> {
                                    ChatProcessor.printFrameworkMessage("Current auth service is \"" + AlteningAPI.currentService().name() + "\"");
                                    return 1;
                                })
                                .then(
                                        LiteralArgumentBuilder.literal("set")
                                                .then(
                                                        LiteralArgumentBuilder.literal("Mojang")
                                                                .executes(c -> {
                                                                    try {
                                                                        AlteningAPI.switchService(AltService.EnumAltService.MOJANG);
                                                                        ChatProcessor.printFrameworkMessage("Set auth service to Mojang.");
                                                                    } catch (Exception ex) {
                                                                        ex.printStackTrace();
                                                                    }
                                                                    return 1;
                                                                })
                                                )
                                                .then(
                                                        LiteralArgumentBuilder.literal("TheAltening")
                                                                .executes(c -> {
                                                                    try {
                                                                        AlteningAPI.switchService(AltService.EnumAltService.MOJANG);
                                                                        ChatProcessor.printFrameworkMessage("Set auth service to TheAltening.");
                                                                    } catch (Exception ex) {
                                                                        ex.printStackTrace();
                                                                    }
                                                                    return 1;
                                                                })
                                                )
                                )
                )
        );
    }

}
