package me.deftware.thealtening;

import me.deftware.client.framework.command.CommandRegister;
import me.deftware.client.framework.main.EMCMod;
import me.deftware.client.framework.wrappers.IMinecraft;
import me.deftware.client.framework.wrappers.gui.IGuiScreen;
import me.deftware.thealtening.ui.IGuiAltening;

public class Main extends EMCMod {

    public static Main instance;
    public static EventListener eventListener;

    @Override
    public void initialize() {
        instance = this;
        eventListener = new EventListener();
        if (getSettings().hasNode("key")) {
            new Thread(() -> AlteningAPI.init(getSettings().getString("key", ""))).start();
        }
        CommandRegister.registerCommand(new CommandAltening());
    }

    @Override
    public void callMethod(String method, String caller) {
        if (method.equals("ui()")) {
            IMinecraft.setGuiScreen(new IGuiAltening(null));
        }
    }

    @Override
    public void callMethod(String method, String caller, Object parent) {
        if (method.equals("ui()")) {
            IMinecraft.setGuiScreen(new IGuiAltening((IGuiScreen) parent));
        }
    }

}
