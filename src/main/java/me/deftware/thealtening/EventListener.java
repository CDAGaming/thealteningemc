package me.deftware.thealtening;

import me.deftware.client.framework.event.EventHandler;
import me.deftware.client.framework.event.events.EventRender3D;
import me.deftware.client.framework.wrappers.IMinecraft;
import me.deftware.client.framework.wrappers.gui.IGuiScreen;

public class EventListener extends me.deftware.client.framework.event.EventListener {

    public IGuiScreen screen = null;

    @EventHandler(eventType = EventRender3D.class)
    public void onRender3D(EventRender3D event) {
        if (screen != null) {
            IMinecraft.setGuiScreen(screen);
            screen = null;
        }
    }

}
