package me.deftware.thealtening.ui;

import com.thealtening.AltService;
import com.thealtening.domain.Account;
import me.deftware.client.framework.FrameworkConstants;
import me.deftware.client.framework.utils.ChatColor;
import me.deftware.client.framework.utils.render.SkinRenderer;
import me.deftware.client.framework.wrappers.IMinecraft;
import me.deftware.client.framework.wrappers.ISession;
import me.deftware.client.framework.wrappers.entity.IEntityPlayer;
import me.deftware.client.framework.wrappers.gui.IGuiButton;
import me.deftware.client.framework.wrappers.gui.IGuiPasswordTextField;
import me.deftware.client.framework.wrappers.gui.IGuiScreen;
import me.deftware.client.framework.wrappers.render.IFontRenderer;
import me.deftware.thealtening.AlteningAPI;
import me.deftware.thealtening.Main;

@SuppressWarnings("Duplicates")
public class IGuiAltening extends IGuiScreen {

    private boolean free = false;
    private IGuiPasswordTextField passwordBox;

    public IGuiAltening(IGuiScreen parent) {
        super(parent);
    }

    @Override
    protected void onInitGui() {
        if (!AlteningAPI.initialized) {
            if (Main.instance.getSettings().getBool("free", false)) {
                free = true;
            } else {
                IMinecraft.setGuiScreen(new IGuiSetup(parent));
                return;
            }
        }
        this.clearButtons();
        this.addButton(new IGuiButton(0, getIGuiScreenWidth() / 2 + 70, getIGuiScreenHeight() / 2 + 55, 120, 20,
                AlteningAPI.currentService() == AltService.EnumAltService.THEALTENING ? ChatColor.GREEN + "Service: TheAltening" : ChatColor.RED + "Service: Mojang") {
            @Override
            public void onButtonClick(double mouseX, double mouseY) {
                ((IGuiButton) getButtonList().get(0)).setButtonText("Changing service...");
                new Thread(() -> {
                    try {
                        if (AlteningAPI.currentService() == AltService.EnumAltService.MOJANG) {
                            AlteningAPI.switchService(AltService.EnumAltService.THEALTENING);
                        } else {
                            AlteningAPI.switchService(AltService.EnumAltService.MOJANG);
                        }
                        ((IGuiButton) getButtonList().get(0)).setButtonText(AlteningAPI.currentService() == AltService.EnumAltService.THEALTENING ? ChatColor.GREEN + "Service: TheAltening" : ChatColor.RED + "Service: Mojang");
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }).start();
            }
        });
        if (!free) {
            this.addButton(new IGuiButton(1, getIGuiScreenWidth() / 2 + 70, getIGuiScreenHeight() / 2 + 80, 120, 20,
                    "Generate account") {
                @Override
                public void onButtonClick(double mouseX, double mouseY) {
                    new Thread(() -> {
                        try {
                            ((IGuiButton) getButtonList().get(1)).setButtonText("Logging into account..");
                            if (AlteningAPI.currentService() == AltService.EnumAltService.MOJANG) {
                                AlteningAPI.switchService(AltService.EnumAltService.THEALTENING);
                                ((IGuiButton) getButtonList().get(0)).setButtonText(AlteningAPI.currentService() == AltService.EnumAltService.THEALTENING ? ChatColor.GREEN + "Service: TheAltening" : ChatColor.RED + "Service: Mojang");
                            }
                            AlteningAPI.currentAccount = AlteningAPI.genAccount();
                            AlteningAPI.login(AlteningAPI.currentAccount);
                            ((IGuiButton) getButtonList().get(1)).setButtonText("Generate account");
                        } catch (Exception ex) {
                            ex.printStackTrace();

                        }
                    }).start();
                }
            });
        } else {
            this.addButton(new IGuiButton(1, getIGuiScreenWidth() / 2 + 70, getIGuiScreenHeight() / 2 + 80, 120, 20,
                    "Redeem token") {
                @Override
                public void onButtonClick(double mouseX, double mouseY) {
                    new Thread(() -> {
                        try {
                            ((IGuiButton) getButtonList().get(1)).setButtonText("Logging into account..");
                            if (AlteningAPI.currentService() == AltService.EnumAltService.MOJANG) {
                                AlteningAPI.switchService(AltService.EnumAltService.THEALTENING);
                                ((IGuiButton) getButtonList().get(0)).setButtonText(AlteningAPI.currentService() == AltService.EnumAltService.THEALTENING ? ChatColor.GREEN + "Service: TheAltening" : ChatColor.RED + "Service: Mojang");
                            }
                            AlteningAPI.login(passwordBox.getTextboxText());
                            ((IGuiButton) getButtonList().get(1)).setButtonText("Redeem token");
                            AlteningAPI.currentAccount = new Account();
                            passwordBox.setTextboxText("");
                        } catch (Exception ex) {
                            ex.printStackTrace();

                        }
                    }).start();
                }
            });
            this.addButton(new IGuiButton(0, getIGuiScreenWidth() / 2 + 70, getIGuiScreenHeight() / 2 + 30, 120, 20, "Get alt") {
                @Override
                public void onButtonClick(double mouseX, double mouseY) {
                   IGuiScreen.openLink("https://thealtening.com/free/free-minecraft-alts");
                }
            });
            passwordBox = new IGuiPasswordTextField(0, getIGuiScreenWidth() / 2 + 70 - 200 - 5, getIGuiScreenHeight() / 2 + 80, 200,
                    20);
            this.addEventListener(passwordBox);
        }
        int buttonWidth = 100;
        addButton(new IGuiButton(0, this.getIGuiScreenWidth() - buttonWidth - 17, 10,
                buttonWidth + 10, 20, "Back") {
            @Override
            public void onButtonClick(double mouseX, double mouseY) {
                IMinecraft.setGuiScreen(parent);
            }
        });
        addButton(new IGuiButton(0, this.getIGuiScreenWidth() - buttonWidth - 17, 35,
                buttonWidth + 10, 20, free ? "Premium" : "Free/Change key") {
            @Override
            public void onButtonClick(double mouseX, double mouseY) {
                Main.instance.getSettings().deleteNode("free");
                Main.instance.getSettings().deleteNode("key");
                Main.instance.getSettings().saveConfig();
                IMinecraft.setGuiScreen(new IGuiSetup(parent));
            }
        });
    }

    @Override
    protected void onDraw(int mouseX, int mouseY, float partialTicks) {
        this.drawITintBackground(0);

        if (FrameworkConstants.PATCH >= 3) {
            SkinRenderer.drawAltBody(IEntityPlayer.getName(), 30, 40, 128 / 2, 256 / 2);
        }

        IFontRenderer.drawStringWithShadow("Status: " + (AlteningAPI.currentAccount == null ? ChatColor.RED + " Not logged into an account" : ChatColor.GREEN + " Logged into an account"), 130, 70, 0xFFFFFF);
        IFontRenderer.drawStringWithShadow("Username: " + (AlteningAPI.currentAccount == null ? "Not logged into account" : ISession.getIUsername()), 130, 80, 0xFFFFFF);

        if (!free) {
            IFontRenderer.drawStringWithShadow("Token: " + (AlteningAPI.currentAccount == null ? "Not logged into account" : AlteningAPI.currentAccount.getToken()), 130, 90, 0xFFFFFF);
            IFontRenderer.drawStringWithShadow("Expires: " + (AlteningAPI.currentAccount == null ? "Not logged into account" : AlteningAPI.currentAccount.getExpiryDate()), 130, 100, 0xFFFFFF);
        } else {
            IFontRenderer.drawStringWithShadow("Alt token:", getIGuiScreenWidth() / 2 + 70 - 200 - 5, getIGuiScreenHeight() / 2 + 65, 0xFFFFFF);
            passwordBox.onDraw(mouseX, mouseY, partialTicks);
        }

        this.drawCenteredString(ChatColor.GREEN + "" + ChatColor.BOLD + "TheAltening" + (free ? " Free" : " Premium"), getIGuiScreenWidth() / 2, 20, 0xffffff);
    }

    @Override
    protected void onUpdate() {
        if (free) {
            passwordBox.doCursorTick();
            getIButtonList().get(1).setEnabled(!passwordBox.getTextboxText().equals(""));
        }
    }

    @Override
    protected void onKeyPressed(int keyCode, int action, int modifiers) {
        if (free) {
            passwordBox.onKeyPressed(keyCode, action, modifiers);
        }
    }

    @Override
    protected void onMouseReleased(int mouseX, int mouseY, int mouseButton) {
        // TODO Auto-generated method stub

    }

    @Override
    protected void onMouseClicked(int mouseX, int mouseY, int mouseButton) {
        if (free) {
            passwordBox.onMouseClicked(mouseX, mouseY, mouseButton);
        }
    }

    @Override
    protected void onGuiResize(int w, int h) {
        // TODO Auto-generated method stub

    }

}
