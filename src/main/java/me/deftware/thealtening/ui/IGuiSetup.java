package me.deftware.thealtening.ui;

import me.deftware.client.framework.utils.ChatColor;
import me.deftware.client.framework.wrappers.IMinecraft;
import me.deftware.client.framework.wrappers.gui.IGuiButton;
import me.deftware.client.framework.wrappers.gui.IGuiPasswordTextField;
import me.deftware.client.framework.wrappers.gui.IGuiScreen;
import me.deftware.thealtening.AlteningAPI;
import me.deftware.thealtening.Main;

public class IGuiSetup extends IGuiScreen {

    private IGuiPasswordTextField passwordBox;

    public IGuiSetup(IGuiScreen parent) {
        super(parent);
    }

    @Override
    protected void onInitGui() {
        this.clearButtons();
        passwordBox = new IGuiPasswordTextField(0, getIGuiScreenWidth() / 2 - 100, getIGuiScreenHeight() / 2 - 25, 200,
                20);
        this.addEventListener(passwordBox);
        this.addButton(new IGuiButton(0, getIGuiScreenWidth() / 2 - 100, getIGuiScreenHeight() / 2 + 50, 200, 20,
                "Finish") {
            @Override
            public void onButtonClick(double mouseX, double mouseY) {
                String key = passwordBox.getTextboxText();
                Main.instance.getSettings().saveString("key", key);
                Main.instance.getSettings().saveConfig();
                AlteningAPI.init(key);
                IMinecraft.setGuiScreen(new IGuiAltening(parent));
            }
        });
        this.addButton(
                new IGuiButton(3, getIGuiScreenWidth() / 2 - 100, getIGuiScreenHeight() / 2 + 75, 100 - 3, 20, "Back") {
                    @Override
                    public void onButtonClick(double mouseX, double mouseY) {
                        IMinecraft.setGuiScreen(null);
                    }
                });
        this.addButton(
                new IGuiButton(3, getIGuiScreenWidth() / 2 + 3, getIGuiScreenHeight() / 2 + 75, 100 - 3, 20, "Free version") {
                    @Override
                    public void onButtonClick(double mouseX, double mouseY) {
                        Main.instance.getSettings().saveBool("free", true);
                        Main.instance.getSettings().saveConfig();
                        IMinecraft.setGuiScreen(new IGuiAltening(parent));
                    }
                });
    }

    @Override
    protected void onDraw(int mouseX, int mouseY, float partialTicks) {
        this.drawITintBackground(0);

        passwordBox.onDraw(mouseX, mouseY, partialTicks);

        this.drawCenteredString("API token:", getIGuiScreenWidth() / 2, getIGuiScreenHeight() / 2 - 45, 0xffffff);
        this.drawCenteredString(ChatColor.GREEN + "" + ChatColor.BOLD + "Setup TheAltening", getIGuiScreenWidth() / 2, 35, 0xffffff);

        this.drawCenteredString(ChatColor.GRAY + "Please enter the API token found at your " + ChatColor.GREEN + "TheAltening" + ChatColor.GRAY + " dashboard", getIGuiScreenWidth() / 2, getIGuiScreenHeight() / 2 + 10, 0xffffff);
        this.drawCenteredString(ChatColor.GRAY + "If you do not have one, go to " + ChatColor.GREEN + "https://thealtening.com/" + ChatColor.GRAY + " to register and get one", getIGuiScreenWidth() / 2, getIGuiScreenHeight() / 2 + 20, 0xffffff);
        this.drawCenteredString(ChatColor.GRAY + "Use special offer code " + ChatColor.GREEN + ChatColor.BOLD + "Aristois" + ChatColor.RESET + ChatColor.GRAY + " for " + ChatColor.GREEN + "20% off" + ChatColor.GRAY + " any plan", getIGuiScreenWidth() / 2, getIGuiScreenHeight() / 2 + 30, 0xffffff);
    }

    @Override
    protected void onUpdate() {
        passwordBox.doCursorTick();
        getIButtonList().get(0).setEnabled(passwordBox.getTextboxText().length() == 18);
    }

    @Override
    protected void onKeyPressed(int keyCode, int action, int modifiers) {
        passwordBox.onKeyPressed(keyCode, action, modifiers);
    }

    @Override
    protected void onMouseReleased(int mouseX, int mouseY, int mouseButton) {
        // TODO Auto-generated method stub

    }

    @Override
    protected void onMouseClicked(int mouseX, int mouseY, int mouseButton) {
        passwordBox.onMouseClicked(mouseX, mouseY, mouseButton);
    }

    @Override
    protected void onGuiResize(int w, int h) {
        // TODO Auto-generated method stub

    }

}
