package me.deftware.thealtening;

import com.thealtening.AltService;
import com.thealtening.TheAltening;
import com.thealtening.domain.Account;
import com.thealtening.utilities.SSLVerification;
import me.deftware.client.framework.utils.SessionUtils;

public class AlteningAPI {

    public static boolean initialized = false;
    private static TheAltening theAltening;
    private static SSLVerification sslVerification = new SSLVerification();
    private static AltService altService = new AltService();
    public static Account currentAccount = null;

    public static void init(String apiKey) {
        if (initialized) {
            return;
        }
        initialized = true;
        theAltening = new TheAltening(apiKey);
    }

    public static AltService.EnumAltService currentService() {
        return altService.getCurrentService();
    }

    public static void switchService(AltService.EnumAltService type) throws Exception {
        if (type == AltService.EnumAltService.MOJANG) {
            altService.switchService(AltService.EnumAltService.MOJANG);
        } else {
            sslVerification.verify();
            altService.switchService(AltService.EnumAltService.THEALTENING);
        }
    }

    public static Account genAccount() throws Exception {
        return theAltening.generateAccount(theAltening.getUser());
    }

    public static void login(Account account) {
        login(account.getToken());
    }

    public static void login(String token) {
        SessionUtils.loginWithPassword(token, "null");
    }

}
